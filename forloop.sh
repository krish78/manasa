#! /bin/bash
#echo -e "enter number here: \c"
#read i 
for (( i=0; i<4; i++ ))
do 
echo $i
done

#for lopp to use list of linux commands

for command in ls pwd date
do 
echo "---------$command--------"
$command
done

#using for loop and if-then both at same time to have all the directories

for dir in *
do 
if [ -d $dir ]
then 
echo "$dir"
fi
done  

